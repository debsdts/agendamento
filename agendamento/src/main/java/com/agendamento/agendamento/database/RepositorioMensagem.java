package com.agendamento.agendamento.database;

import com.agendamento.agendamento.Entidades.Mensagem;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositorioMensagem  extends JpaRepository<Mensagem, Long>{
    
}
