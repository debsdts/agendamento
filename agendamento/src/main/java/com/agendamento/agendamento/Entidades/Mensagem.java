
package com.agendamento.agendamento.Entidades;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.agendamento.agendamento.Enum.EnumTipo;
import com.agendamento.agendamento.Enum.StatusTipo;

@Entity
public class Mensagem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EnumTipo tipo;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private StatusTipo status;
    @Column(nullable = false)
    @NotBlank(message = "Campo destinatario é obrigatorio")
    private String destinatario;
    @Column(nullable = false)
    @NotBlank(message = "Campo mensagem é obrigatorio")
    private String mensagem;
    @Column(nullable = false)
    private LocalDateTime dataHora;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public EnumTipo getTipo() {
        return tipo;
    }

    public void setTipo(EnumTipo tipo) {
        this.tipo = tipo;
    }

    public StatusTipo getStatus() {
        return status;
    }

    public void setStatus(StatusTipo status) {
        this.status = status;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }

}
