package com.agendamento.agendamento;

import java.util.List;
import com.agendamento.agendamento.Entidades.Mensagem;
import com.agendamento.agendamento.database.RepositorioMensagem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/mensagem")
public class MensagemRest {

    @Autowired
    private RepositorioMensagem repos;

    @GetMapping
    public List<Mensagem> listar(){
        return repos.findAll(); 
    }

    @PostMapping
    public void salvar (@RequestBody Mensagem mensagem){
        repos.save(mensagem);
    }

    @PutMapping
    public void alterar (@RequestBody Mensagem mensagem){
        if (mensagem.getId() > 0)
            repos.save(mensagem);
    }

    @DeleteMapping
    public void excluir (@RequestBody Mensagem mensagem){
        repos.delete(mensagem);
    }
    ; 
}
